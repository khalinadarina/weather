import React from "react";
import { useFetchForecast } from "../hooks/useFetchForecast";
import WeatherModel from "../models/WeatherModel";
import { ForecastDay } from "./ForecastDay";

interface ForecastProps {}

export const Forecast: React.FC<ForecastProps> = (props) => {
    const { data } = useFetchForecast();

    console.log('>>', data);

    const days = data?.data.slice(0, 7).map((weather: WeatherModel) =>
        <ForecastDay key={weather.id} weather={weather} />
    );

    return (
        <div className="forecast">
            {days}
        </div>
    );
};