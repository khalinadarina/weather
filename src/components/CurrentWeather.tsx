interface CurrentWeatherProps {}

export const CurrentWeather: React.FC<CurrentWeatherProps> = (props) => {
    return (
        <div className="current-weather">
            <p className="temperature">11</p>
            <p className="meta">
                <span className="rainy">%60</span>
                <span className="humidity">%30</span>
            </p>
        </div>
    );
}