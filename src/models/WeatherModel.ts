interface WeatherModel {
    id: string,
    date: string,
    values: {
        humidity: number,
        windSpeed: number,
        windDirection: number,
        temperature: number,
        cloudCover:	number,
        weatherIconCode: number,
        weather: string,
    }
}

export default WeatherModel;
